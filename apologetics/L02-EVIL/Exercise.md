I will write down 10 sins/evils the Bible mentions, and you the student tell me:
- is it a lack (carencia), privation (falta) or corruption (corrupcion) ?
- then of **what** is it a lack, privation, or corruption of ?
	- i.e. what is the good that is no longer good in these sins/evils

1. Disobedience = Desobediencia (Genesis 3:6)

2. Murder = Homicidio (Genesis 4:8)

3. Evil desires (lust would be a subset) = Deseos malos (Genesis 5:5)

4. Pride = Orgullo (Proverbs 16:5)

5. Idolatry = Idolatria (Exodo 20:4)

6. Greed/Covetousness = Codicia (Exodo 20:17)

7. Envy = Envidia (James 3:16)

8. Gluttony = Glotoneria (Filipenses 3:19)

9. Wrath/Anger = Ira/Enojo (Salmo 37:8)

10. Sodomy = Sodomia (Levitico 18:22)

11. Abortion = Aborto (Exodo 20:13)

12. The Crucifixion ?