**Last paragraph from Ch 2**
Of course, explaining evil as a lack in a good thing does not explain **where the lack comes from**. All it explains is the nature of evil as a real privation in good things. **Where the corruption came from** is another question - one treated in the next chapter.

**Question**
One of the most asked questions to Christians is "Where did evil come from?", Why is there so much suffering, and pain, and loss, and grief, in a world that was supposedly created by an absolutely good and powerful God !
- Did it come from God?
	- absolutely not because God is absolutely good therefore only good things could come from God
- Did it come from perfect beings, the ones God created?
	- How can they do evil if they are perfect beings?

**God**
- Only created good things.

**Evil**
- Evil is a lack, privation, corruption of/in a good thing.
- No substances (things) in and of themselves are evil.
- But then the question is:
	- Where then did evil come from? What was the starting point? The cause?
	- How did the lack, privations, corruptions get there?

# The problem of Evil's Origin
How can absolute good be the source of evil?
THIS IS A GREAT MYSTERY.

**Problem's Summary**
1. God is absolutely perfect.
2. God cannot create anything imperfect.
3. A perfect creature cannot do evil.
4. Therefore, evil cannot arise in such a world.
5. But evil did arise in this world.
6. Hence it seems that either premise 1 or 2 (or both) is false, i.e.:
	1. God is not absolutely perfect
	2. God did not create perfect creatures - or
	3. Both a. and b. are true

**Problem Questioned in Biblical Terms**
Why didn't God create better creatures - creatures who would not sin?
Why didn't God just create creatures that would not sin, that would not fall, that would not rebel,
the types of creatures that are in heaven now and that will be in heaven afterwards, the saints?

**Response to the Problem of Evil's Origin**
1st two premises seem solid:
1. God is absolutely perfect.
2. God cannot create anything imperfect.
- Note: Finitude is not necessarily imperfect, infinitude cannot be created

But look at the 3rd premise:
3. And a perfect creature cannot do evil.

Is this true ?

This is false actually, because God created a perfect archangel, called Lucifer; and he sinned (1 Timothy 3:6) and became the satan/devil.

So, *How can a perfect creature do evil?*
The answer is *free will*.

1. God created only good things.
2. One good thing God created was free will.
3. Free will makes evil possible, since
  a. It is the power to do otherwise.
  b. To do otherwise than good is evil.
4. Hence a perfect free creature can do evil.

Evil cannot come directly from the Hand of the Creator who is absolutely good.

We also cannot deny that free will (the power of free choice) is a good thing. If free will were evil,
then it would be evil for God to have made it, evil for the saints in heaven for eternity to have it.

Freedom is good, Adam and Eve were free before they disobeyed.
We are set free by and through Jesus. And in eternity with Him we will be free from all evil, sin, malice, suffering, pain, etc.

If it is good to be free, then evil is possible. Since freedom means the power to choose otherwise.

If we are free to love, we are free to hate.
If we are free to praise God, we are free to curse God.
The very nature of our divinely given freedom makes evil possible.
Any alleged "freedom" NOT TO CHOOSE EVIL rather than good is not
really freedom for a moral creature.

God absolutely has the freedom not to choose evil, but the saints have it relatively.
The highest freedom is freedom from evil, not the freedom of doing evil.

Dustin Benge said "Sinners see sinning as freedom. Saints see sinning as bondage."

We understand as disciples of Jesus that sinning, committing evil is not true freedom. True freedom
is being able to live like and for Christ through His Word.

**Who caused Lucifer to sin?**
According to the Bible, Lucifer (isaiah 14:12, cf. 1 Timothy 3:6), a created archangel, was the first to sin,
thus becoming Satan, which literally means adversary in hebrew.

How could sin arise under these conditions:
Perfect God, perfect Heaven and Creation, Perfect angels in Heaven!

WHO CAUSED LUCIFER TO SIN?

Lucifer was:
- not tempted by anyone else, otherwise the tempter would have been the first one to sin
- God certainly did not tempt Lucifer (JAMES 1:13 MUST READ)
- Lucifer had no evil nature that gave him a propensity (inclination) to sin.

This is a great mystery. There have been many answers to this question/issue/problem.
The answer we are looking at today is free will.

Now, let's look at the **basis of a free choice**:
A free act can be:
1. Uncaused
2. Caused by another
3. Self-caused

or in other terms
1. Undetermined
2. Determined by another
3. Self-determined

1. Uncaused or Undetermined
- Is this possible?
  - for an action to be uncaused or undetermined is illogical, why?
  - it violates a basic law of logic: the law of causality
    - every effect/event has a cause, otherwise there would be no effect/event

2. Determined by another
  - if someone or something else caused your action, then it is not ours (not from our free choice) and we would not be responsible for it

3. Self-caused
  - all free actions must be self-caused or self-determined, caused by oneself

So if we ask the question again:
Who caused Lucifer to sin? what would you now say ?

- Answer: No one caused Lucifer to sin, it was self-caused

Sin is a self-caused action, one for which we cannot blame anyone or anything else.

Lucifer was the cause of his own sin, and he caused it because of free choice.

God made evil **possible** by creating free creatures; they are responsible for making it **actual** !

God does not sin, nor does He encourage anyone else to do so.
Lucifer sinned on his own, by his own free will.

**How did evil arise in a totally good universe?**
So the Christian's apologia or defense of free will would be like this:
1. A good creature (Lucifer)
2. With the good power of free will,
3. Willed the finite good of the creature (himself)
4. Over the infinite good of the Creator.

It is important to note than no evil need exist in order to will evil; for example, willing a lesser good CAN BE AN EVIL.

The evil of willing oneself to take the place of God is an evil in itself.

1 Timothy 3:6 Paul warns Timothy of placing neophytes or new converts in office, otherwise they could become
conceited and experience the same judgement as satan

Ezekiel 28:13-17
Isaiah 14:12-14

Sin was born in the breast of an archangel in the presence of God.

Luke 10:18 Jesus saw Satan fall

When Lucifer made himself the object of adoration, he placed himself before God, he fell, he sinned, he committed evil.

Sin is a self-caused action, caused by oneself.

It is meaningless to ask, "Who caused Lucifer to sin?" as it is to ask "Who made God?"

No one made God, the Unmade Maker

And no one caused Lucifer to sin, He is the maker of his own sin.
