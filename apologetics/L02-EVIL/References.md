## References

[GTY - Is God responsible for evil?](https://www.gty.org/library/articles/A189/is-god-responsible-for-evil)
[GTY - The Problem of Evil](https://www.gty.org/library/blog/B170116)
[GTY - The Truth about Evil](https://www.gty.org/library/blog/B170117)
