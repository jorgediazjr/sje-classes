## Why is ther Evil
### by RC Sproul
### Chapter 1
A theodicy - from the greek meaining "to justify God" - is an attempt to justify
God for the existence of evil in the world. But in my opinion, no one has yet been able to
adequately answer the problem of evil.

The first thing a Christian must do when he's confronted with this question is to immediately say
"I don't know the answer," and acknowledge the seriousness of the question.

When we choose, we always choose according to our strongest inclination, our strongest desire.
That's what the essence of choice is: the mind choosing.
I do something because that's what I want to do.
That's what it means to make a choice.
It's an action proceeding from a desire.

3 possible inclinations for Adam and Eve:
- Evil
  - this means they were fallen already
- Neutral
  - there would be no moral action, because you need a cause for the action to take place, for a moral action to be
  it cannot be arbitrary. it is not random. no moral disposition. without a disposition to choose, you cannot choose.
- Good
  - if their disposition was good only then how did they choose evil

### Chapter 2

The what is the easy of of the question. But the hard part is the origin!!!

John Stuart Mill argued against the existence of God because of the evil he saw.

To answer the question of evil's origin, it demands an adequate theodicy.
A theodicy is an intellectual, reasoned defense of God for the problem of evil in the universe.

Sin/Evil entered the world through Adam.

Before choices are made, there has to be some kind of inclination.

It is true that Lucifer made a choice, and that choice was to do evil.
It is also true that Adam and Eve made a choice, and that choice was to do evil.

But, before that choice was actualized, why was their inclination towards a lesser good, an evil, if there was no evil
in them. Where did that prior disposition come from? What inclined Adam and Eve to disobey God?

If we say that there was no prior inclination in Adam and Eve to choose evil, then we have eliminated
moral agency in them, their choice was no moral action at all.

If Adam and Eve were coerced into sinning, if satan coerced both of them to disobey God,
if it was determined by another, then they would be excused, because their action would not
have been a free one.
- if they were really coerced, they would have no power to resist, no power to choose good
- but this is not Sciptural, Adam and Eve were not coerced, they were guilty of sinning against God

If Eve disobeyed God because she was tricked, and she did it in ignorance, then she and Adam would
have not been guilty before God, and God's exiling them from paradise would have been unjust.

Adam and Eve were clearly instructed by God not to eat of the fruit of the tree of the knowledge of Good and Evil
They knew the consequences.

Adam and Eve, physically, mentally, spiritually, entirely, were much stronger, smarter, wiser, better than us
because their being came into existence in a completely good state and no sin/evil had affected their total being yet
like it affects us humans.

God said:
If you do A, then B

Satan said:
If you do A, then NOT B

This is the law of non-contradiction

Doing A cannot result in B and not-B, it can only be one.

Example:
If you jump off a building, you will fall.
If you jump off a building, you will NOT fall.

Adam and Eve's ability to think clearly far exceeded Albert Einstein, Aquinas, Paul the
Apostle, Socrates, because their minds were pure, unaffected by any evil because evil was
not yet so in their lives.

Satan's deception of our human forefathers did not excuse them, because they were morally
capable and culpable for recognizing the contradiction to the Word of God and obeying the
lie rather than the truth.

When we despise the law of God, which is Good, when we say there's something wrong with
His Law, we are calling good evil. Evil is not good, but it is good that there is evil.
Otherwise, evil would not be in a universe ruled by a perfect God.

God is not saying that the bad things we suffer are good things; He says that they are
**working** for good. God uses them ultimately for good.

As Joseph said to his brothers in Genesis 50:20, "As for you, you meant evil against me,
but God meant it for good."

God has an ability to order the universe in which He uses evil for perfectly pure
and holy purposes, which we will see fully only in glory.

The presence of evil indirectly points to the reality of the good. It becomes an argument
not against God but for the existence of God.

If an unbeliever denies the existence of God, then they must deny the objective existence
of good, and of evil, and admit that there are only personal preferences to what is good and
what is evil.

Though I don't now or fully understand the origin of evil, I do know the future of it.
I do know that it has been overcome and that God will rid this universe of all moral evil,
physical evil, and metaphysical evil, as we grow up into the fulness of Christ and inhabit
a new heaven and a new earth, where there will be no more crying, no more sin, and no more
death.
