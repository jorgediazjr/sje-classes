### Theodicies

When people, unbelievers, speak against God's existence and base their unbelief,
denial, rejection of God on the reality of evil, and say horrible things about our
Creator, usually causes Christians to step in and speak on God's behalf to correct
these unbelievers!
When Christians try to explain the reality of God, who is all-good,
all-loving, and all-powerful, 
and the reality of evil, suffering, pain, etc, this explanation would be known as a
theodicy !

We will be going over 3 theodicies and mentioning the one from last class again.

What is a theodicy (teodicea) ?
It is a combination of 2 greek words, Theos or Theo which is greek for God,
and Dikaios which means justification in greek.
So a theodicy is literally a God-justification.

Talking about evil in any reasonable sesnse requires moral absolutes, and in
a universe where there is no God, then there are no moral absolutes.

Why would a perfectly good being be inclined to choose evil over good?
Where did that impulse come from?
And why would God permit it?

There is not a full answer in the Bible with respect to the origin of evil,
but it does talk a lot about it, God doesn't ignore that reality.

A definition from the cambridge dictionary on theodicy is:
the question of how God can exist when there is evil in the world,
or a good reason or explanation for this

It is also a branch of philosophy that deals with the issue of evil in
light of the existence of God.

Simply, an attempt to justify God for the existence of evil in the world.

Some theodicies have denied the reality of evil altogether. We already saw
that pantheism denies the reality of evil but also christian science, saying
that it is an illusion.

Some theodicies say that evil is a necessary prerequisite for the the apreciation
of the good, and so in the final analysis evil is good.

1. For man to really experience goodness in freedom, he had to experience the
problem of evil so that he might appreciate his redemption.
2. In this theodicy, the fall was not really going downward but rather going upward.

It was not getting away from God but actually coming closer to Him.

If this theodicy is true (and it isn't), it wouldn't explain God's judgement that
he executed upon, Adam, Eve, the Serpent, and the curse on the earth for
the entrance of sin into the world by Adam and Eve.

----------------------------------------------------------------
Free Will from Norman Geisler Theodicy from Last Class

This is the most common theodicy Christians hold, affirm, or use
to answer the problem of evil ?

This is why we gave it last class.

----------------------------------------------------------------

Student's Theodicies from Homework

----------------------------------------------------------------

David Snoke

The bottom line is that the Christian does not believe that a good God must be absolutely and unremittingly dedicated to not allowing evil to exist even for a moment.

# How can God allow it?
Some people have argued, however, that while we do not know everything, we can know with certainty that there is no possible justification for allowing the evil that exists in the world.

All the Christian needs to argue is that it is at least plausible that there is some good purpose for allowing an evil. If there is at least some plausible reason, then the argument that there is no possible justification fails.

What are some plausible reasons?
The basic Christian response can be called the “greater good” argument: a temporary evil can be tolerated if it leads to an ultimate greater good that outweighs the evil.

Therefore it is plausible that there is some greater good outcome to a greater temporary evil, even if we don’t know what that greater good is.

There is a second argument against the “greater good” position, however. This is the “ends don’t justify the means.” Is the “greater good” justification a violation of this ethic? Does God use evil means to get a good end?

# Natural Evil and Moral Evil
Natural evil is something that is physically unpleasant, possibly extremely and excruciatingly so. This could include pain and disease, hurricanes, floods, famines, parasites, etc. All these types of things can happen to us independent of any moral choices that humans make. We might make them worse by bad moral choices, but they would exist anyway.

We just define whatever God does as good and not evil. But if we believe that God is eternal and unchanging in his character, we would like an absolute standard for good and evil, which we believe he follows consistently.

“Good” is hard to define: it involves every good thing such as beauty, order, existence, life, meaning, etc. To make an exhaustive list of good would be a lengthy task. But assuming we have some intuitive sense of the good, we can define moral evil as that which seeks to destroy the good.

We can therefore define moral evil as an action done with the motivation to destroy good as an end in itself; or more generally, to pursue a lesser good over a greater good as an end in itself.

but God condemns evil people and evil spirits. He holds them accountable as beings who make choices, who can see that some choices are loving and others are evil, and yet choose evil.

# What is the ultimate cause of moral evil?
Our thinking on this topic is colored by the general premise, shared by many Christians, that to cause moral evil to exist is to do moral evil.

This is sometimes presented as God voluntarily limiting his omnipotence, to allow independent causes beyond his control, namely choices by moral beings.

These theologians would say that moral evil comes to each human as the result of the original moral choice of Adam and Eve.

A “first cause” is something that directly causes something else. For example, God’s spoken command was the first cause of the creation of the world, and has been the first cause of various miracles

A “second cause” is something which may be caused by something else, but also causes something to happen.

It is a basic doctrine of Christianity that God is separate from his creation.

Using this language, then, we can say that God is the ultimate, or first, cause of all things that happen.

But in saying this, it does not follow that God is the immediate actor, or “owner,” of everything that happens.

While we can say “That moral being chose evil” it does not follow to say “God chose evil.”

If we define evil as seeking the destruction of the good, or seeking a lesser good over a greater good, as an end in itself, then it is clear that moral beings like people can do evil, but their evil does not (at least not intrinsically) imply that God does evil.

If God makes evil people come into existence for some greater, ultimate purpose (which we may not know), then he is not doing evil.

The pure act of destroying a good thing can’t be intrinsically evil. As discussed above, this would imply that all change is evil. If a good thing is replaced by another good thing (e.g., a caterpillar which is replaced by a butterfly), then the first good thing is eliminated, or destroyed.

It is common to argue that if my parents caused me to have certain patterns of action, or if my physical body has made me have feelings of rage or irrationality, or if economic oppression has led me to have constrained choices, then I am not guilty.

The Christian view is that God has given to the creation the power to do things and to cause things to happen.

Simple creations can do simple things, e.g., rocks can smash things, and animals can bite things. Moral beings have an additional causal power which may be called “creativity.”

A moral being can create a new thing. In the positive sense, this means that people can create artwork, imagine plans for new machines and buildings, and create language.

The first example of this is Adam’s naming of the animals. Genesis 2 says that God brought the animals to Adam “to see what he would call them”
(Genesis 2:19). God gave Adam the ability to make names, made Adam everything that he was, and in his omniscience knew what Adam would say even before Adam existed, but the names came from Adam.

In the negative sense, this creative ability means that moral agents can lie. Lying involves an act of imagination to create a scenario which is not real, but it involves an additional act of attempting to present the lie as the truth. At its core, every evil deed starts with a lie: to say, even to ourselves, that we want what is best, when in fact we don’t.

In all kinds of cases we can see forces which influenced or shaped a creative act (including a lie), but we still recognize the ownership that a creative/moral being has over its acts of creativity.

In summary, God is the ultimate cause of all things, including evil, but is not the immediate cause, or “author” of evil.

God has the power to create beings with creative ability of their own.

As such, they are the “authors” of their own deeds.

It is no more appropriate to say that God is author of their sins than it is to say that God, and not Gabriel Garcia Marquez, is the author of 100 años de soledad.

If we say that God is not the ultimate cause of sin, we are saying that God is not the creator of all things in the universe; there are some things (e.g., sin) that have their origin apart from him.

If he does not end it immediately, it must be for reasons of a greater good, which are precisely the same reasons which we must invoke if God caused the process directly.

----------------------------------------------------------------
Gottfried Leibniz Theodicy
- Some christian people have used this theodicy to convince other people that
Christians do have an explanation for the origin of sin

Leibniz starts with a 3-fold distinction with respect to evil:
1. The 3 distinctions are moral evil, physical evil, and metaphysical evil.

There is a common meaning in these 3 distinctions,
evil is defined in negative categories as some kind of lack.
This goes back to the definition that we looked at where evil
is defined as a lack, deficiency, privation, of the good.

Moral evil is a lack of moral good.
- it's a deficiency

Physical evil is a deficiency of physical good.

Metaphysical evil is a deficiency of metaphysical goodness.


Moral evil has to do with the actions of moral creatures (human beings),
the volitional behavioral patterns of moral agents.

Physical evil would be those things that we describe in terms of
calamity or tragedy: earthquakes, tornadoes, fire, wind storm,
and pestilence.

Metaphysical evil has to do with ontological imperfection.
Ontology concerns "being", the essence of things.
- an ontological matter is what something is, its being, its essence.

Metaphysical imperfection is to be less than an eternal self-existent being,
to be less than ultimate.

An imperfect thing would be that which is created and dependent, that which
undergoes change, generation, and decay.

In a word, that which is metaphysically lacking is that which is finite.

Leibniz basic thesis is that physical evil "flows out of"
  metaphysical evil,

and moral evil "flow out of" physical evil.

So now, what is the reason we have moral evil?
It is because the world is full of
metaphysically imperfect beings.

The reason I sin is because I am weak.
The reason that I am weak is because I am finite.

And the only way I could be without sin is if:
- I could transcend the instrinsic metaphysical weakness that associates itself
with finite creatures

By definition, we are not all-powerful, we are not all-wise, we are not all the things
that God is.
So there is a sense in which it is inevitable that out of my simple human weakness I would sin.

What do you guys think of that ??

But a question remains: Why would God create such a limited, weak, finite creature?
- Leibniz understood that the fact that God creates at all is a benevolent act on His part
- so we can't blame God for wanting to create

But if God is going to give the gift of life, this gift of being, to other creatures, how can He best do it?

Why doesn't He create man morally perfect?

This is the judgement the skeptics have raised.

If God is going to create man, why doesn't He create him perfectly good?

Leibniz' answer is that God can't. That God cannot create a perfectly good creature,
because to create him perfectly good morally, He would also have to create him
perfectly good physically, and to create him perfectly good physically, He would
have to create him perfectly good metaphysically, and that would be impossible,
because He would have to create another God. Another God who is infinite,
eternal, self-existent, and complete in His being.

Is it possible for God to create another God ?
NO ! Because whatever God creates would be dependent on
the first God for existence. It would not be eternal.
It would not have self-existence. It would be inferior
ontologically to the Creator who brought it into being
in the first place.

So God cannot create another God, but God could create an almost infinite number of different kinds of being.

The issue is not "Must God create a perfect world?" We can't demand that He create a perfect world.

But if God is moral, if He is righteous, we can demand that God create the best of all possible worlds.

The best of all possible worlds is what this theodicy is also known as.

Problems with Leibniz theodicy:
1. Intellectual Problem
- Leibniz has committed one of the most basic errors of reasoning, but it is one of the
most difficult kinds of errors of reasoning to discern.
- He's committed the fallacy of equivocation.
- In each of these distinctions, the meaning of the term evil changes.

Moral evil carries with it the notion of that which deserves punitive measures.

Moral evil by definition is the kind of evil that comes out of volitional creatures.

Metaphysical evil, on the other hand, excuses man from being morally evil.
It offers man and God an excuse.

It not only justifies God for the existence of evil, but it also justifies man for the existence of evil.

Man cannot really be held responsible because it's necessary for him to sin because of his
metaphysical imperfection.

And if it's necessary for men to sin, how can we stand in judgement for doing what we must do by nature?

1. Biblical problem
- first: if the theodicy is correct, then it would be impossible for us to be free of moral evil in heaven,
unless God does more than glorify us; He must deify us to make us free of evil.
- second: it means that Adam never falls
  - adam was created evil, at least metaphysically evil and physically evil, which excuses him morally.

Leibniz argument cannot function as a Christian theodicy for the problem of evil.

----------------------------------------------------------------

The essence of freedom is self-determination, rather than being determined
by something or someone outside my self.

Freedom means doing what I want to do.

The act is determined by the person making the choice.

It is not enough to say that because man was free to choose good or not-good, he chose
not-good.

We must ask why did he chose not-good. What inclination was there before the choice was made?
Good disposition, bad disposition, neutral disposition?

The church throughout history has agreed that the disposition of man was good, and yet
he chose to do something not-good.

That's the mystery.


DEFINITIONS:
fallacy of equivocation -> falacia de equívoco
- se repite la palabra pero en cada aparición su significado es distinto.
Ejemplos:
- No siempre es rosa(color) la rosa(planta/flor)
- Ni se te ocurra echarle sal(preservativo) y sal(accion) de mi cocina

