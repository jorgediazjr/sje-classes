## Chapter 5
## The Purpose of Evil

We don't always see God's purposes worked out so quickly on earth.
Sometimes we won't see until we reach the other side.
But God has His purposes nonetheless.

The Purpose of Evil's Question can be put in many different ways:
- What is the significance of suffering?
- What is the meaning of misery?
- What is the cause for calamity?
- Or what is the purpose of pain?

### Problem: There seems to be no apparent purpose for some evil
The question WHY lingers in our mind when we experience what we would call purposeless pain.
Put in a logical form, it would be as follows:
1. An all-good God must have a good purpose for everything.
2. But there is no good purpose for some (i.e., useless or innocent) suffering.
3. Hence there cannot be an all-good God.

For premise 1, Christians affirm that.
Because God cannot have a BAD purpose for anything, and so that leaves that He must have a good purpose for anything.
If He had at least one purpose that was bad out of billions and billions of good purposes, God would no longer be all-good.

For premise 2, there is a problem.
It is incorrect to say that
1) Our KNOWING no purpose is the same as saying
2) There actually BEING no purpose

### Response to the argument from the apparent purposelessness for some evil
- The fact that we do NOT know a good purpose for some evil does NOT mean that there is no good purpose for it.

It should be expected that in the future we will discover good purposes for things for which we do not now know a good purpose.

The unexplained does not necessarily mean unexplainable.

We cannot commit the mistake of not knowing a good purpose for some evil, and saying therefore that evil has no good purpose at all.

### It should not be expected that we know the purpose for everything
- As creatures with finite minds, we cannot see the future, we only know so much, it should not be expected that we know all the good purposes for every evil that happens.
  - it is not expected for us to know the real purpose for absolutely everything
  - Deuteronomy 29:29 The secret things belong to the Lord our God
  - Romans 11:33 Paul marveled at God "How unsearchable are His judgments and how inscrutable His ways!"
  - Job 42:3 Job spoke what he did not know nor understand

### An Infinitely Good Mind knows a Good purpose for everything
- We cannot assert that there is no good purpose for some suffering just because we do not know
- We can assert that God, Who is Infinite in Knowledge and Wisdom does know a good purpose for every evil, pain, suffering
- We can also assert that because God is All-Good then He has a good purpose for everything He does OR PERMITS

In its logical form:
1) That we do not know a good purpose for evil does not mean there is none.
2) An all-good God knows a good purpose for everything (including evil).
  a) some evil seems to us to have no good purpose
  b) but an all-good God has a good purpose for everything
  c) So even evil that seems to have no good purpose does have a good purpose
3) Therefore, there is a good purpose for all suffering, even that which we cannot now explain.

### Things once unexplained but now explained
1. The unexplained is not unexplainable.
2. Many things scientists once could not explain (e.g. earthquakes, meteors, hurricanes, and tornados), they can now explain.

Example: Joseph did not know why he was sold as a slave into Egypt by his brothers.
Later, however, he was able to say to them: "You meant evil against me, but God meant it for good, to bring it about that many people should be kept alive, as they are today" (Genesis 50:20)

Read Hebrews 12:10-11

### A Place for reasonable faith
- Even though we do not always know why, at least we know why we do not know why - because we are limited in our knowledge.
  - this requires trust from us to God, because we know He knows it all, and that He is all-good

Job 1:21

We have good reason to believe that God has a good purpose for allowing the suffering He allows.

### We do know some good purposes for pain and suffering
- There are many things we suffer for which we do know a good purpose.
E.g. A toothache is a good pain that warns us of something worse and gives us a chance to correct the problem.
There are many lessons we can learn from pain.

### Lesson One: Pain is designed to keep us from self-destruction
- Medical science has discovered that the body's nervous that conveys pain to us is designed to save our lives.
  - leprosy: most of the loss of fingers and toes is not caused by the disease but by the leper himself
    - it destroys the ability to sense pain
    - the leper has no warning when he/she is in dangerous situations that can cause harm or even death to his body
    - e.g. you could be severely burned, and not know it if you cannot feel pain

### Lesson Two: In order to save us from self-destruction the pain has to be strong enough
- Experiments were done with lepers where they were equipped with bleeping devices to warn of pain, but the bleep was not painful at all, so it did not make them stop from self-destruction

### Lesson Three: In order for pain to work it has to be out of our control
- A shock mechanism did not work either, the lepers would just turn it off for certain situations where they knew they'd
receive a shock
- None of us will to go through suffering, and yet most of us admit we are better persons for having done so.

The truth is that we learn more enduring lessons through pain than we do pleasure.

God is more concerned with our holiness than with our happiness, our character more than our comfort.

Read James 1:2-3

### What God does through allowing suffering
- Read Romans 5:3-4

### Problems with evil's reality
- We view humans to highly, and we view God to lowly
  - we do not realize nor understand the depth of our own evil, and we do not realize nor understand the greatness of God's holiness

### Remember Theodicy
- Justifying the ways of God before men.

There exists anthropodicy which is justifying man.

How many of you believe humans are good?

Nice != Goodness

What do you think you deserve? Heaven or Hell?

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|-|
Suffering on this earth

vs

|------------------------------------>
Glory forever with Jesus

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We are not as good as we might think we are.
Everytime we chose evil, we are choosing something not good, we are communicating with our actions that our evil choice is actually good and what is actually good is evil.

Humans are really the problem because humans constantly do evil around the world, we do it, our friends do it.

If we say that we were born better than others, we are talking like those who did genocide!

The crucifixion of Jesus Christ was the greatest evil ever committed in human history but it was at the same time glorious.
- the attributes of God displayed on the Cross

If you are tired of evil, then hate it and turn away, and turn towards Victor over evil, Jesus Christ, and love His Return!
- 2 Timothy 4:8

Look at what the Bible says about God:
- 1 John 3:8
- 1 John 1:5

This is a passage that is really close to answer the purpose of evil!
- Romans 9:22-23
In other words, what if God permitted sin so that we might know the fullness of His glory?
For without sin and suffering we would never know God’s forgiveness, grace, mercy, holiness, righteousness, compassion, and love.
It is by the presence of sin and evil that we see the glory of God shine most brightly.

- Read Acts 2:22-23; 4:24, 28
- Read Romans 8:28
- Read Ephesians 1:11

God's sovereignty: The fact that God is sovereign essentially means that He has the power, wisdom, and authority to do anything He chooses within His creation.
- The sovereignty of God is the same as the lordship of God, for God is the sovereign over all of creation. The major components of God’s lordship are his control, authority, and covenantal presence.
- Psalm 135:6


### Glossary
Purpose -->


### Translations
Purposelessness --> 
Body's Nervous System --> 
Bleeping device --> 
Genocide --> 
